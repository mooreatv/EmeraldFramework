local EF
if EFrame == nil then 
    EFrame = {}
end
EF = EFrame

setmetatable(EF, {__index = getfenv() })
setfenv(1, EF)

rootFrame = rootFrame or CreateFrame("frame")

local function weakTable()
    local t = {}
    setmetatable(t, {__mode = 'v'})
    return t
end

rootFrame:SetScript("OnUpdate", function (this)
    this.update()
    EFrame:disposeGarbage()
end)

local bindLoop

function EFrame:atomic(f, ...)
    if not self.lock then
        local ret
        self.lock = true
        ret = f(...)
        self:process()
        self.lock = nil
        return ret
    else
        return f(...)
    end
end

function EFrame:process()
    local n = tremove(self.procced,1)
    EFrame:disposeGarbage()
    while n do
        n()
        EFrame:disposeGarbage()
        n = tremove(self.procced,1)
    end
end

collector = {}
objects = weakTable()

local statsCache = {total = 0, garbage = {}}
function EFrame:stats()
    local t = statsCache
    for k in pairs(t) do
        if k ~= "garbage" then
            t[k] = 0
            t.garbage[k] = 0
        end
    end
    t.garbage.total = 0
    for _,o in pairs(self.objects) do
        if o._destroyed then
            t.garbage[o.type] = (t.garbage[o.type] or 0) +1
            t.garbage.total = t.garbage.total +1
        else
            t[o.type] = (t[o.type] or 0) +1
            t.total = t.total +1
        end
    end
    return t
end

function EFrame:addObject(o)
    tinsert(objects,o)
end

function EFrame:addGarbage(o)
    tinsert(collector, o)
end

function riterator(t, n)
    n = n -1
    if n > 0 then
        return n, t[n]
    end
end

function ripairs(t)
    return riterator, t, #t +1
end

function EFrame:disposeGarbage()
    for _, o in pairs(collector) do
        o:destroy()
    end
    for k in ripairs(collector) do
        tremove(collector,k)
    end
end

function EFrame:makeAtomic(f)
    return function (...)
        return self:atomic(f, ...)
    end
end

function bind(f)
    return Bind(f)
end

function EFrame:enqueue(f)
    tinsert(self.procced, f)
end

local function query(self, f, k)
    if type(f) == "table" then
        return f[k]
    else
        return f(self, k)
    end
end

local function getRecycled(t)
    local ret = tremove(t)
    if ret then
        ret._destroyed = nil
        return ret
    end
    local fresh = {}
    EFrame:addObject(fresh)
    return fresh
end

local function recycle(o)
    o._destroyed = true
    local tt = EFrame[o.type]
    tinsert(tt.recycle, o)
end

local function destroy(o)
    o._destroyed = true
end

local function callNew(self, ...)
    local obj = getRecycled(self.recycle)
    setmetatable(obj, self._MT)
    self.new(obj, ...)
    return obj
end

local function build(self, k, p)
    if type(p) == "table" and p.build then
        p.build(self, unpack(p))
        p = rawget(self, k)
        assert(p)
    end
    return p
end

local function objIndex(self, idx, k)
    local pn = '_'..k
    local p = rawget(self, pn) or build(self, pn, idx[pn])
    if getmetatable(p) == Property._MT then
        return p:_get(self, k.."Changed")
    end
    return rawget(self, k) or build(self, k, idx[k])
end

local function objNewIndex(self, idx, k, v)
    local pn = "_"..k
    local p = rawget(self, pn) or build(self, pn, idx[pn])
    local old
    if getmetatable(p) ~= Property._MT then
        return rawset(self, k, v)
    end
    local oldbind = p.bind
    if oldbind and oldbind == v then
        return
    end
    if olbind then
        old = p:get()
        if not olbind and old == v then
            return
        end
    end
    if getmetatable(v) == Property._MT then
        local bp = v
        v = bind(function() return bp:_get() end)
    end
    if getmetatable(v) == Bind._MT then
        v.parent = self
        v._p = p
        v:update()
        p.bind = v
    else
        p.bind = nil
        p:set(v)
    end
    if oldbind then
        oldbind:destroy()
    end
end

function newClass(type, super)
    if rawget(getfenv(2),type) then error(format("type \"%s\" already exists.", type)) end
    getfenv(2)[type] = {type = type}
    local tt = getfenv(2)[type]
    if super then
        setmetatable(tt, { __call = callNew, __index = super })
    else
        setmetatable(tt, { __call = callNew })
    end
    if type == "Object" or super and super.isObject then
        tt._MT = {
            __index = function(self, k) return objIndex(self, tt, k) end,
            __newindex = function(self, k, v) objNewIndex(self, tt, k, v) end }
    else
        tt._MT = { __index = tt }
    end
    tt.recycle = weakTable()
end

newClass("Array")

newClass("Object")

newClass("Connection")

newClass("Signal")

newClass("Property")

function Array:new(t)
    if t then
        self[0] = t[0]
        self[0][1] = self[0][1] +1
    else
        self[0] = {0}
    end
end

function Array:detach()
    local old = self[0]
    old[1] = old[1] - 1
    self[0] = {}
    for k, v in pairs(old) do
        rawset(self[0], k, v)
    end
    self[0][1] = 0
end

function Array:modifiable()
    if self[0][1] > 0 then
        self:detach()
    end
    return self[0]
end

function Array:insert(e)
    self:modifiable()[e] = e
end

function Array:remove(i)
    self:modifiable()[i] = nil
end

function Array:iter()
    local i, t = pairs(self[0])
    return i, t, 1
end

function Array:riter()
    return ripairs(self[0])
end

function Array:destroy()
    self[0][1] = self[0][1] - 1
    recycle(self)
end

function Connection._MT.__call(self, ...)
    if self.lambda then
        self.target(...)
    else
        if self.ququed then
            local args = {...}
            EFrame:enqueue(function() EFrame:atomic(self.target[self.slot],self.target, unpack(args)) end)
        else
            return self.target[self.slot](self.target, ...)
        end
    end
end

function Connection:new(signal, target, slot)
    self.target = target
    self.slot = slot
    self.signal = signal
    self.lambda = type(self.target) == "function"
end

function Connection:disconnect()
    if type(self.target) == "table" then
        self.target._connections[self] = nil
    end
    self.signal:disconnect(self)
    destroy(self)
end

function Signal._MT.__call(self, ...)
    local locked
    if not EFrame.lock then
        EFrame.lock = true
        locked = true
    end
    local auto = self.parent and self.autoslot and self.parent[self.autoslot]
    if auto then
        auto(self.parent, ...)
    end
    if self.connections then
        local conns = Array(self.connections)
        for k, c in conns:iter() do
            c(...)
        end
        conns:destroy()
    end
    if locked then
        EFrame:process()
        EFrame.lock = false
    end
end

function Signal:new(parent, name)
    self.parent = parent
    self.autoslot = format("on%s%s",strupper(strsub(name,1,1)),strsub(name,2)or"")
end

function Signal:destroy()
    self:disconnectAll()
    if self.connections then
        self.connections:destroy()
    end
    destroy(self)
end

function Signal:connect(target, slot)
    local t1, t2 = type(target), type(slot)
    if not (t1 == "table" and t2 == "string" or t1 == "function" and not slot) then error(format("Cannot connect to types (<%s,%s>)",t1,t2)) end
    local c = Connection(self, target, slot)
    if not self.connections then
        self.connections = Array()
    end
    self.connections:insert(c)
    if t1 == "table" then
        target._connections[c] = c
    end
    return c
end

function Signal:disconnect(target, slot)
    if not self.connections then return end
    if type(target) == "table" and target.type == "Connection" then
        self.connections:remove(target)
    end
    for k,c in self.connections:iter() do
        if type(c) == "table" and c.target == target and c.slot == slot then
            self.connections:remove(c)
            break
        end
    end
end

function Signal:disconnectAll()
    if not self.connections then return end
    for _,c in self.connections:iter() do
        if type(c) == "table" then
            c:disconnect()
        end
    end
end

function Property:new(parent, name, meta)
    if meta then
        if meta.getter then
            self.get = meta.getter
        end
        if meta.setter then
            self.set = meta.setter
        end
    end
    self.parent = parent
    self.name = name
    self.signal = name.."Changed"
    return self
end

function Property:get()
    return self.parent["__"..self.name]
end

local binding
function Property:_get(o, sn)
    if binding then
        local s = (o or self.parent)[sn or self.signal]
        if s then
            binding:bind(s)
        end
    end
    return self:get()
end

function Property:set(v)
    local name = "__"..self.name
    if v ~= self.parent[name] then
        self.parent[name] = v
        local s = rawget(self.parent, self.signal)
        if s then
            return s(v)
        end
    end
end

function Property:destroy()
    for k, v in pairs(self) do
        self[k] = nil
    end
    self._destroyed = true
end

Object.isObject = true
    
function Object:new(meta, parent)
    self._connections = {}
    self._values = {}
    self._signals = weakTable()
    self._properties = weakTable()
    if meta then
        if meta.signals then
            for _,s in pairs(meta.signals) do
                self:attachSignal(s)
            end
        end
        if meta.properties then
            for _,s in pairs(meta.properties) do
                self['_'..s] = Property(self, s)
                self:attachSignal(s.."Changed")
            end
        end
    end
    self:attachSignal("destroyed")
    Object._parent.build(self, unpack(Object._parent))
    self.parent = parent
    self.__children = {}
end

function Object:deleteLater()
    EFrame:addGarbage(self)
end


function Object:destroy()
    local t = self.type
    assert(not self._destroyed, "Attempt to destroy object twice.")
    self.destroyed(self)
    self.destroyed:disconnectAll()
    local c = tremove(self.children)
    while c do
        c:destroy()
        c = tremove(self.children)
    end
    if self.__parent then
        self.__parent:removeChild(self)
    end
    local c = tremove(self._connections)
    for k,c in pairs(self._connections) do
        c:disconnect()
    end
    for k, v in pairs(self._signals) do
        v:destroy()
    end
    for k, v in pairs(self._properties) do
        v:destroy()
    end
    
   -- self.parent = nil
    
    destroy(self)
end

Property.ReadOnly = "READONLY"

function Object:attach(name, getter, setter, implicit)
    if setter == Property.ReadOnly then
        setter = function() error(format("cannot make assignment to readonly property %s."),name) end
    end
    local meta = {
        getter = getter and function ()
            return self[getter](self)
        end,
        setter = setter and function (_, v)
            self[setter](self,v)
        end
    }
    local p = Property(self, name, meta)
    rawset(self, '_'..name, p)
    tinsert(self._properties, p)
    local sn = name.."Changed"
    if not implicit and not self._signals[sn] then
        self:attachSignal(sn)
    end
end

function Object:attachMeta(name, getter, setter)
    self["_"..name] = {name, getter, setter, true, build = Object.attach, n = 4}
    self:attachSignalMeta(name.."Changed")
end

function Object:attachSignal(n)
    local s = Signal(self, n)
    self._signals[n] = s
    rawset(self, n, s)
end

function Object:attachSignalMeta(name)
    self[name] = {name, build = Object.attachSignal }
end

function Object:setParent(parent)
    local oldparent = self.__parent
    if parent == oldparent then return end
    if oldparent then
        oldparent:removeChild(self)
    end
    self.__parent = parent
    if parent then
        parent:addChild(self)
    end
    self.parentChanged(parent)
end

function Object:addChild(f)
    tinsert(self.__children, f)
    self:childrenChanged(self.__children)
end

function Object:removeChild(f)
    for k,c in pairs(self.__children) do
        if c == f then
            tremove(self.__children, k)
            self:childrenChanged(self.__children)
            return
        end
    end
end

Object:attachMeta("children",nil, Property.ReadOnly)
Object:attachMeta("parent", nil, "setParent")

newClass("Bind", Object)

function Bind:new(f, p)
    Object.new(self)
    local t = type(f)
    if t == "string" then
        f = loadstring(format("return function(self)return %s end", f))()
    end
    self._f = f
    self._p = p
    self._s = {}
end

function Bind:update()
    local bindOwn
    if not bindLoop then
        bindOwn = true
        bindLoop = {}
    end
    if self._p then
        if not bindLoop[self._p] then
            bindLoop[self._p] = (bindLoop[self._p] or 0) + 1
        else
            -- print("Binding loop detected for "..self._p.parent.type..":"..self._p.signal .. " / " .. bindLoop[self._p])
        end
    end
    for k in pairs(self._s) do
        self._s[k] = 0
    end
    local old = binding
    binding = self
    local v = self._f(self.__parent)
    binding = old
     for k, c in pairs(self._connections) do
         if self._s[c.signal] ~= 1 then
             c:disconnect()
             self._s[c.signal] = nil
             self._connections[k] = nil
         end
     end
    if self._p then
        self._p:set(v)
    end
    if bindOwn then
        bindLoop = nil
    end
    return v
end

function Bind:bind(s)
     if not self._s[s] then
        s:connect(self, "update")
     end
     self._s[s] = 1
end

function Bind:drop()
    for k,c in pairs(self._connections) do
        c:disconnect()
    end
end

function Bind:destroy()
    self:drop()
    Object.destroy(self)
end

rootFrame.update = Signal(EFrame, "update")
procced = {}
