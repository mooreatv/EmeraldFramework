local EF
if EFrame == nil then 
    EFrame = {}
end
EF = EFrame

setmetatable(EF, {__index = getfenv() })
setfenv(1, EF)

local function fuzzyCmp(a, b)
    return not (a or b) or (a and b) and math.abs(a - b) < 0.001
end

local function round(x)
    return math.floor(x+0.5)
end

local _,_,resx, resy = strfind(({GetScreenResolutions()})[GetCurrentResolution()],"^(%d+)x(%d+)$")
frameRecycler = {}
objectRecycler = {}
rootFrame = rootFrame or CreateFrame("frame")
function EFrame:recycle(f)
    if not f.owned then return end
    f:SetParent(nil)
    f:ClearAllPoints()
    f:SetAlpha(1)
    local t = strlower(f:GetObjectType())
    if not self.frameRecycler[t] then
        self.frameRecycler[t] =  {}
    end
    tinsert(self.frameRecycler[t],f)
end
function EFrame:recycleObject(f)
    f:Hide()
    f:ClearAllPoints()
--    f:SetParent(nil)
    local t = strlower(f:GetObjectType())
    self.objectRecycler[t] = self.objectRecycler[t] or {}
    tinsert(self.objectRecycler[t],f)
end

function EFrame:newFrame(t, parent)
    t = strlower(t)
    local f = self.frameRecycler[t] and tremove(self.frameRecycler[t]) or CreateFrame(t)
    f:SetParent(parent)
    f.owned = true
    return f
end

function EFrame:newObject(t,p,m)
    t = strlower(t)
    local r = self.objectRecycler[t] and tremove(self.objectRecycler[t])
    if not r then
        return p[m](p)
    end
    r:SetParent(p)
    r:Show()
    return r
end

newClass("VirtualItem", Object)

VirtualItem.isItem = true

local function colorComp(c1,c2)
    return not (c1 or c2) or c1 and c2 and c1[1] == c2[1] and c1[2] == c2[2] and c1[3] == c2[3] and c1[4] == c2[4]
end

VirtualItem:attachMeta("_bottom")
VirtualItem:attachMeta("_right")
VirtualItem:attachMeta("_vcentre")
VirtualItem:attachMeta("_hcentre")
VirtualItem:attachMeta("marginLeft")
VirtualItem:attachMeta("marginTop")
VirtualItem:attachMeta("marginRight")
VirtualItem:attachMeta("marginBottom")
VirtualItem:attachMeta("hcentreOffset")
VirtualItem:attachMeta("vcentreOffset")
VirtualItem:attachMeta("rotation")
VirtualItem:attachMeta("implicitWidth")
VirtualItem:attachMeta("implicitHeight")
VirtualItem:attachMeta("width", nil,"setWidth")
VirtualItem:attachMeta("height", nil,"setHeight")
VirtualItem:attachMeta("visible", nil, "setVisible")
VirtualItem:attachMeta("y", nil, "setY")
VirtualItem:attachMeta("x", nil, "setX")
VirtualItem:attachMeta("realY", nil,"setRealY")
VirtualItem:attachMeta("realX", nil,"setRealX")
VirtualItem:attachMeta("z","getZ","setZ")
VirtualItem:attachMeta("anchorTop")
VirtualItem:attachMeta("anchorBottom")
VirtualItem:attachMeta("anchorVCentre")
VirtualItem:attachMeta("anchorLeft")
VirtualItem:attachMeta("anchorRight")
VirtualItem:attachMeta("anchorHCentre")
VirtualItem:attachMeta("anchorFill")
VirtualItem:attachMeta("margins", nil, "setAllMargins")
VirtualItem:attachMeta("opacity", "getOpacity", "setOpacity")
VirtualItem:attachMeta("visibleChildren")
VirtualItem:attachMeta("childrenItems", nil, Property.ReadOnly)
VirtualItem:attachMeta("keyboardEnabled", "getKeyboardEnabled", "setKeyboardEnabled")
VirtualItem:attachMeta("scale", nil, "setScale")
VirtualItem:attachSignalMeta("keyPressed")
VirtualItem:attachSignalMeta("keyReleased")

VirtualItem.__implicitWidth = 0
VirtualItem.__implicitHeight = 0
VirtualItem.__margins = 0
VirtualItem.__marginTop = 0
VirtualItem.__marginBottom = 0
VirtualItem.__marginRight = 0
VirtualItem.__marginLeft = 0
VirtualItem.__hcentreOffset = 0
VirtualItem.__vcentreOffset = 0
VirtualItem.__scale = 1
VirtualItem.__height = 0
VirtualItem.__width = 0


function VirtualItem:new(parent)
    self.__childrenItems = {}

    assert(not parent or parent.isItem)
    Object.new(self)
    self.left = {frame = self, point = "x"}
    self.right = {frame = self, point = "_right"}
    self.top = {frame = self, point = "y"}
    self.bottom = {frame = self, point = "_bottom"}
    self.hcentre = {frame = self, point = "_hcentre"}
    self.vcentre = {frame = self, point = "_vcentre"}
    self.update = update
    self.parent = parent
    self.x = 0
    self.y = 0
    self.realX = bind(function() local s = self.scale return (self.x + self.width * (1 - s)/2) / s end)
    self.realY = bind(function() local s = self.scale return (self.y + self.height * (1 - s)/2) / s end)
    self.width = self._implicitWidth
    self.height = self._implicitHeight
    self._bottom = bind("self.y + self.height")
    self._right = bind("self.x + self.width")
    self._vcentre = bind("self.y + self.height/2")
    self._hcentre = bind("self.x + self.width/2")
    self.visible = true
    self.visibleChildren = bind(Item.getVisibleChildren)
    self.vanchorBind = bind(VirtualItem.updateVericalAnchors)
    self.vanchorBind.parent = self
    self.vanchorBind:update()
    self.hanchorBind = bind(VirtualItem.updateHorizontalAnchors)
    self.hanchorBind.parent = self
    self.hanchorBind:update()
end

function VirtualItem:setX(x)
    if fuzzyCmp(x, self.__x) then return end
    self.__x = x
    return self.xChanged(x)
end

function VirtualItem:setY(y)
    if fuzzyCmp(y, self.__y) then return end
    self.__y = y
    return self.yChanged(y)
end

function VirtualItem:setParent(p)
    if not p then return Object.setParent(self) end
    Object.setParent(self, p)
    self:setRealX(self.__realX or 0, true)
    self:setRealY(self.__realY or 0, true)
    return self:updateVisible()
end

function VirtualItem:addChild(c)
    Object.addChild(self, c)
    if c.isItem then
        return self:addChildItem(c)
    end
end

function VirtualItem:firstFrame()
    return self.__parent:firstFrame()
end

function VirtualItem:removeChild(c)
    if c.isItem then
        self:removeChildItem(c)
    end
    return Object.removeChild(self, c)
end

function VirtualItem:addChildItem(c)
    tinsert(self.__childrenItems, c)
    return self.childrenItemsChanged(self.__childrenItems)
end

function Object:removeChildItem(f)
    for k,c in pairs(self.__childrenItems) do
        if c == f then
            tremove(self.__childrenItems, k)
            return self:childrenItemsChanged(self.__childrenItems)
        end
    end
end

function VirtualItem:getVisibleChildren()
    local vc = {}
    for _,c in pairs(self.childrenItems) do
        if c.visible then
            tinsert(vc, c)
        end
    end
    return vc
end

function VirtualItem:updateVisible()
    local old = self.__visible
    if self.__parent and self.__parent.__visible then
        return self:_setVisible(self:firstFrame():IsShown() and true or false)
    else
        return self:_setVisible(false)
    end
end

function VirtualItem:setRealX(x, force)
    local d = fuzzyCmp(x, self.__realX)
    if not force and d then return end
    self.__realX = x
    for _,c in pairs(self.__childrenItems) do
        c:setRealX(c.__realX, true)
    end
    if not d then return self.realXChanged(x) end
end

function VirtualItem:setRealY(y, force)
    local d = fuzzyCmp(y, self.__realY)
    if not force and d then return end
    self.__realY = y
    for _,c in pairs(self.__childrenItems) do
        c:setRealY(c.__realY, true)
    end
    if not d then return self.realYChanged(y) end
end

function VirtualItem:setFrameX(frame, x)
    return self.__parent:setFrameX(frame, x + self.__realX)
end

function VirtualItem:setFrameY(frame, y)
    return self.__parent:setFrameY(frame, y + self.__realY)
end

function VirtualItem:setZ(z)
end

function VirtualItem:getZ()
    return self.__parent.z
end

function VirtualItem:setWidth(w)
    if fuzzyCmp(self.__width, w) then return end
    
    self.__width = w
    return self.widthChanged(w)
end

function VirtualItem:setHeight(h)
    if fuzzyCmp(self.__height, h) then return end
    self.__height = h
    return self.heightChanged(h)
end

function VirtualItem:_setVisible(v)
    if self.__visible ~= v then
        self.__visible = v
        for _,c in pairs(self.__children) do
            if c.updateVisible then
                c:updateVisible()
            end
        end
        return self.visibleChanged(self.__visible)
    end
end

function VirtualItem:setVisible(t)
    if t then
        self.__visible = true
        return self:_setVisible(self.__parent.__visible and true or false)
    else
        self.__visible = false
        return self:_setVisible(false)
    end
end

function VirtualItem:setAllMargins(v)
    self.marginTop = v
    self.marginBottom = v
    self.marginLeft = v
    self.marginRight = v
end

function VirtualItem:absLeft()
    local p = self.__parent
    return (p and p:absLeft() or 0) + self.__x
end

function VirtualItem:absTop()
    local p = self.__parent
    return (p and p:absTop() or 0) - self.__y
end

function VirtualItem:mapXFrom(item, x)
    local p = self.__parent
    local ip = item.__parent
    if not ip then
        return x - p:absLeft() + item:absLeft()
    end
    return x - p:absLeft() + ip:absLeft()
end

function VirtualItem:mapYFrom(item, y)
    local p = self.__parent
    local ip = item.__parent
    if not ip then
        return y - item:absTop() + p:absTop()
    end
    return y - ip:absTop() + p:absTop()
end

local function anchorX(frame, anchor)
    return frame:mapXFrom(anchor.frame, anchor.frame[anchor.point])
end

local function anchorY(frame, anchor)
    return frame:mapYFrom(anchor.frame, anchor.frame[anchor.point])
end

function VirtualItem:updateVericalAnchors()
    local fill = self.anchorFill
    if fill then
        self.y = anchorY(self, fill.top) + self.marginTop
        self.height = fill.height - self.marginTop - self.marginBottom
        return
    end
    local top, bot, vc = self.anchorTop, self.anchorBottom, self.anchorVCentre
    local y, height, oh
    if not (top or bot or vc) then return end
    if top and bot then
        height = anchorY(self, bot) - anchorY(self, top) - self.marginBottom - self.marginTop
    elseif top and vc then
        height = (anchorY(self, vc) - anchorY(self, top) - self.marginTop + self.vcentreOffset)*2
    elseif bot and vc then
        height = (anchorY(self, bot) - anchorY(self, vc) - self.marginBottom - self.vcentreOffset)*2
    elseif bot or vc then
        height = self.height
        oh = true
    else
        height = self.__height
        oh = true
    end
    if top then
        y = anchorY(self, top) + self.marginTop
    elseif vc then
        y = anchorY(self, vc) - height/2 + self.vcentreOffset
    elseif bot then
        y = anchorY(self, bot) - height - self.marginBottom
    end
    self.y = y
    if not oh then
        self.height = height
    end
end

function VirtualItem:updateHorizontalAnchors()
    local fill = self.anchorFill
    if fill then
        self.x = anchorX(self, fill.left) + self.marginLeft
        self.width = fill.width - self.marginLeft - self.marginRight
        return
    end
    local left, right, hc = self.anchorLeft, self.anchorRight, self.anchorHCentre
    local x, width, ow
    if not (left or right or hc) then return end
    if left and right then
        width = anchorX(self, right) - anchorX(self, left) - self.marginRight - self.marginLeft
    elseif left and hc then
        width = (anchorX(self, hc) - anchorX(self, left) - self.marginLeft + self.hcentreOffset)*2
    elseif right and hc then
        width = (anchorX(self, right) - anchorX(self, hc) - self.marginRight - self.hcentreOffset)*2
    elseif right or hc then
        width = self.width
        ow = true
    else
        width = self.__width
        ow = true
    end
    if left then
        x = anchorX(self, left) + self.marginLeft
    elseif hc then
        x = anchorX(self, hc) - width/2 + self.hcentreOffset
    elseif right then
        x = anchorX(self, right) - width - self.marginRight
    end
    self.x = x
    if not ow then
        self.width = width
    end
end

newClass("Item", VirtualItem)

function Item:new(parent, wrap, mode)

    parent = parent or root
    assert(wrap == rootFrame or parent)
    assert(not parent or parent.isItem)
    self.frame = wrap or EFrame:newFrame("frame")
    self.frame.owned = mode == nil or mode
    VirtualItem.new(self, parent)
--    self.frame:SetScript("OnKeyDown", function(this, button) this:SetPropagateKeyboardInput(true) self.keyPressed(button) end)
--    self.frame:SetScript("OnKeyUp", function(this, button) this:SetPropagateKeyboardInput(true) self.keyReleased(button) end)
end

function Item:destroy()
    self.frame:SetScale(1)
    self.frame:Hide()
    EFrame:recycle(self.frame)
    return VirtualItem.destroy(self)
end

function Item:firstFrame()
    return self.frame
end

function Item:setParent(p)
    if not p then return Object.setParent(self) end
    self.frame:SetParent(p:firstFrame())
    return VirtualItem.setParent(self, p)
end

-- function Item:absLeft()
--     return self.frame:GetLeft() or 0
-- end
-- 
-- function Item:absTop()
--     return self.frame:GetTop() or 0
-- end

function Item:setVisible(t)
    if t then
        self.frame:Show()
        return self:_setVisible(self.frame:IsVisible() and true or false)
    else
        self.frame:Hide()
        return self:_setVisible(false)
    end
end

function Item:getOpacity()
    return self.frame:GetAlpha()
end

function Item:setOpacity(o)
    local old = self:getOpacity()
    if old == o then return end
    self.frame:SetAlpha(o)
    return self.opacityChanged(o)
end

function Item:getKeyboardEnabled()
    return self.frame:IsKeyboardEnabled() == 1
end

function Item:setKeyboardEnabled(e)
    if self.keyboardEnabled == e then return end
    self.frame:EnableKeyboard(e and 1 or 0)
    return self.keyboardEnabledChanged(e)
end

function Object:removeChildItem(f)
    for k,c in pairs(self.__childrenItems) do
        if c == f then
            tremove(self.__childrenItems, k)
            return self:childrenItemsChanged(self.__childrenItems)
        end
    end
end

function Item:getVisibleChildren()
    local vc = {}
    for _,c in pairs(self.childrenItems) do
        if c.visible then
            tinsert(vc, c)
        end
    end
    return vc
end

function Item:setRealX(x, force)
    local d = fuzzyCmp(x, self.__realX)
    if not force and d then return end
    local p = self.__parent
    if p then
        p:setFrameX(self.frame, x)
    else
        self.frame:SetPoint("LEFT", self.frame ~= root and root or nil, "LEFT", round(x), 0)
    end
    self.__realX = x
    if not d then return self.realXChanged(x) end
end

function Item:setRealY(y, force)
    local d = fuzzyCmp(y, self.__realY)
    if not force and d then return end
    local p = self.__parent
    if p then
        p:setFrameY(self.frame, y)
    else
        self.frame:SetPoint("TOP", self.frame ~= root and root or nil, "TOP", 0, -round(y))
    end
    self.__realY = y
    if not d then return self.realYChanged(y) end
end

function Item:setFrameX(frame, x)
    frame:SetPoint("LEFT", self.frame, "LEFT", round(x), 0)
end

function Item:setFrameY(frame, y)
    frame:SetPoint("TOP", self.frame, "TOP", 0, -round(y))
end

function Item:setZ(z)
    if self:getZ() == z then return end
    
    self.frame:SetFrameLevel(z)
    return self.zChanged(z)
end

function Item:getZ()
    return self.frame:GetFrameLevel()
end

function Item:getWidth()
    return self.frame:GetWidth()
end

function Item:setWidth(w)
    if not force and fuzzyCmp(self.__width, w) then return end
    
    self.frame:SetWidth(w)
    if force then return end
    self.__width = w
    return self.widthChanged(w)
end

function Item:getHeight()
    return self.frame:GetHeight()
end

function Item:setHeight(h)
    if not force and fuzzyCmp(self.__height, h) then return end
    self.frame:SetHeight(h)
    if force then return end
    self.__height = h
    return self.heightChanged(h)
end

function Item:setScale(s)
    if self.__scale == s then return end
    self.__scale = s
    self.frame:SetScale(s)
    self:setRealX(self.__realX, true)
    self:setRealY(self.__realY, true)
    return self.scaleChanged(s)
end

newClass("Rectangle", Item)

Rectangle:attachMeta("color", nil,"setColor")
Rectangle:attachMeta("borderColor", nil, "setBorderColor")
Rectangle:attachMeta("borderWidth", nil, "setBorderWidth")
Rectangle:attachMeta("layer", "getLayer", "setLayer")
Rectangle:attachMeta("blendMode", "getBlendMode","setBlendMode")

function Rectangle:new(parent)
    Item.new(self, parent)
    self._texture = EFrame:newObject("texture",self.frame,"CreateTexture")
    self._texture:SetAllPoints()
    self.layer = "ARTWORK"
    self.blendMode = "BLEND"
    self.color = {1,1,1}
end

function Rectangle:destroy()
    EFrame:recycleObject(self._texture)
    self._texture:SetTexture(nil)
    if self._borderTop then
        self._borderTop:SetTexture(nil)
        self._borderBottom:SetTexture(nil)
        self._borderRight:SetTexture(nil)
        self._borderLeft:SetTexture(nil)
        EFrame:recycleObject(self._borderTop)
        EFrame:recycleObject(self._borderBottom)
        EFrame:recycleObject(self._borderRight)
        EFrame:recycleObject(self._borderLeft)
    end
    return Item.destroy(self)
end

function Rectangle:_borderSetup()
    if not self._borderTop then
        self._borderTop = EFrame:newObject("texture", self.frame, "CreateTexture", self:getLayer())
        self._borderBottom = EFrame:newObject("texture", self.frame, "CreateTexture", self:getLayer())
        self._borderRight = EFrame:newObject("texture", self.frame, "CreateTexture", self:getLayer())
        self._borderLeft = EFrame:newObject("texture", self.frame, "CreateTexture", self:getLayer())
        
        self._borderTop:SetPoint("TOP", self.frame,"TOP")
        self._borderTop:SetPoint("LEFT", self.frame,"LEFT")
        self._borderTop:SetPoint("RIGHT", self.frame,"RIGHT")
        
        self._borderRight:SetPoint("RIGHT", self.frame,"RIGHT")
        self._borderRight:SetPoint("TOP", self._borderTop,"BOTTOM")
        self._borderRight:SetPoint("BOTTOM", self._borderBottom,"TOP")
        
        self._borderBottom:SetPoint("BOTTOM", self.frame,"BOTTOM")
        self._borderBottom:SetPoint("LEFT", self.frame,"LEFT")
        self._borderBottom:SetPoint("RIGHT", self.frame,"RIGHT")
        
        self._borderLeft:SetPoint("LEFT", self.frame,"LEFT")
        self._borderLeft:SetPoint("TOP", self._borderTop,"BOTTOM")
        self._borderLeft:SetPoint("BOTTOM", self._borderBottom,"TOP")
    end
end

function Rectangle:setColor(color)
    if colorComp(self.__color, color) then
        return
    end
    self.__color = color
    self._texture:SetColorTexture(unpack(color))
    return self.colorChanged(color)
end

function Rectangle:setBorderColor(color)
    if colorComp(self.__borderColor, color) then
        return
    end
    self:_borderSetup()
    self.__borderColor = color
    self._borderTop:SetColorTexture(unpack(color))
    self._borderRight:SetColorTexture(unpack(color))
    self._borderBottom:SetColorTexture(unpack(color))
    self._borderLeft:SetColorTexture(unpack(color))
    return self.borderColorChanged(color)
end

function Rectangle:setBorderWidth(width)
    if fuzzyCmp(self.__borderWidth, width) then return end
    self:_borderSetup()
    self._texture:ClearAllPoints()
    if width == 0 then
        self._borderTop:Hide()
        self._borderBottom:Hide()
        self._borderRight:Hide()
        self._borderLeft:Hide()
        self._texture:SetAllPoints()
    else
        self._borderTop:SetHeight(width)
        self._borderBottom:SetHeight(width)
        self._borderRight:SetWidth(width)
        self._borderLeft:SetWidth(width)
        self._borderTop:Show()
        self._borderBottom:Show()
        self._borderRight:Show()
        self._borderLeft:Show()
        self._texture:SetPoint("TOP", self._borderTop, "BOTTOM")
        self._texture:SetPoint("RIGHT", self._borderRight, "LEFT")
        self._texture:SetPoint("BOTTOM", self._borderBottom, "TOP")
        self._texture:SetPoint("LEFT", self._borderLeft, "RIGHT")
    end
    self.__borderWidth = width
    return self:borderWidthChanged(width)
end


function Rectangle:getLayer()
    return self._texture:GetDrawLayer()
end

function Rectangle:setLayer(l)
    if self:getLayer() == strupper(l) then
        return
    end
    self._texture:SetDrawLayer(l)
    if self._borderTop then
        self._borderTop:SetDrawLayer(l)
        self._borderBottom:SetDrawLayer(l)
        self._borderRight:SetDrawLayer(l)
        self._borderLeft:SetDrawLayer(l)
    end
    return self.layerChanged(l)
end

function Rectangle:getBlendMode()
    return self._texture:GetBlendMode()
end

function Rectangle:setBlendMode(m)
    if self:getBlendMode() == strupper(m) then
        return
    end
    self._texture:SetBlendMode(m)
    return self.blendModeChanged(m)
end

newClass("Image", Item)

Image:attachMeta("source", "getSource","setSource")
Image:attachMeta("color", nil, "setColor")
Image:attachMeta("layer", "getLayer", "setLayer")
Image:attachMeta("blendMode", "getBlendMode","setBlendMode")
Image:attachMeta("rotation",nil, "setRotation")

function Image:new(parent)
    Item.new(self, parent)
    self._texture = EFrame:newObject("texture",self.frame,"CreateTexture")
    self._texture:SetAllPoints()
    self.layer = "ARTWORK"
    self.blendMode = "BLEND"
end

function Image:destroy()
    self._texture:SetTexture(nil)
    self._texture:SetVertexColor(1,1,1,1)
    self._texture:SetTexCoord(0,1,0,1)
    EFrame:recycleObject(self._texture)
    return Item.destroy(self)
end

function Image:getSource()
    return self._texture:GetTexture()
end

function Image:setSource(src)
    if src == self.source then
        return
    end
    
    self._texture:SetTexture(src)
    return self.sourceChanged(src)
end

local function rotate(this, r)
    function c(x,y)
        local sin = math.sin(r)
        local cos = math.cos(r)
        return (x*cos - y * sin)/2+0.5, -(x*sin + y * cos)/2 + 0.5
    end
    local ulx, uly = c(-1,1)
    local llx, lly = c(-1,-1)
    local urx, ury = c(1,1)
    local lrx, lry = c(1,-1)
    this:SetTexCoord(
        ulx,uly,
        llx,lly,
        urx,ury,
        lrx,lry)
end

function Image:setRotation(r)
    if r == self.rotation then
        return
    end
    
    rotate(self._texture, r)
    return self.rotationChanged(r)
end

function Image:setColor(color)
    if colorComp(self.__color, color) then
        return
    end
    self.__color = color
    self._texture:SetVertexColor(unpack(color))
    return self.colorChanged(color)
end

function Image:getLayer()
    return self._texture:GetDrawLayer()
end

function Image:setLayer(l)
    if self:getLayer() == strupper(l) then
        return
    end
    self._texture:SetDrawLayer(l)
    return self.layerChanged(l)
end

function Image:getBlendMode()
    return self._texture:GetBlendMode()
end

function Image:setBlendMode(m)
    if self:getBlendMode() == strupper(m) then
        return
    end
    self._texture:SetBlendMode(m)
    return self.blendModeChanged(m)
end

function Image:setCoords(...)
    return self._texture:SetTexCoord(...)
end

newClass("Label", Item)

Label:attachMeta("text","getText","setText")
Label:attachMeta("color",nil,"setColor")
Label:attachMeta("shadowColor","getShadowColor","setShadowColor")
Label:attachMeta("shadowOffset","getShadowOffset","setShadowOffset")
Label:attachMeta("outline","getOutline","setOutline")
Label:attachMeta("hAlignment","getHAlignment","setHAlignment")
Label:attachMeta("vAlignment","getVAlignment","setVAlignment")
Label:attachMeta("contentWidth","getContentWidth",Property.ReadOnly)
Label:attachMeta("contentHeight","getContentHeight",Property.ReadOnly)

function Label:new(parent)
    Item.new(self, parent)
    self.n_text = EFrame:newObject("fontstring",self.frame,"CreateFontString")
    self.n_text:SetFontObject("GameFontNormal")
    self.n_text:SetAllPoints()
    self.implicitWidth = self._contentWidth
    self.implicitHeight = self._contentHeight
    self.text = ""
    self.color = {GameFontNormal:GetTextColor()}
    self.visibleChanged:connect(self, "refreshContentSize")
end

function Label:destroy()
    EFrame:recycleObject(self.n_text)
    return Item.destroy(self)
end

function Label:getText()
    return self.n_text:GetText()
end

function Label:setText(t)
    if self:getText() == t then return end
    
    self.n_text:SetText(t)
    self.textChanged(t)
    self:getContentWidth()
    self:getContentHeight()
end

function Label:setColor(color)
    if colorComp(self.__color, color) then
        return
    end
    self.__color = color
    self.n_text:SetTextColor(unpack(color))
    return self.colorChanged(color)
end

function Label:getShadowColor()
    return {self.n_text:GetShadowColor()}
end
    
function Label:setShadowColor(c)
    local old = self:getShadowColor()
    if colorComp(old, c) then return end
    self.n_text:SetShadowColor(unpack(c))
    return self.shadowColorChanged(c)
end

function Label:getContentWidth()
    local v = self.n_text:GetStringWidth()
    if not fuzzyCmp(v, self.__contentWidth) then
        self.__contentWidth = v
        self.contentWidthChanged(v)
    end
    return v
end

function Label:getContentHeight()
    local v = self.n_text:GetStringHeight()
    if not fuzzyCmp(v, self.__contentHeight) then
        self.__contentHeight = v
        self.contentHeightChanged(v)
    end
    return v
end

function Label:refreshContentSize()
    self:getContentWidth() self:getContentHeight()
end

function Label:getHAlignment()
    return self.n_text:GetJustifyH()
end

function Label:setHAlignment(a)
    if strupper(a) == self:getHAlignment() then return end
    self.n_text:SetJustifyH(a)
    return self:hAlignmentChanged(a)
end

function Label:getVAlignment()
    return self.n_text:GetJustifyV()
end

function Label:setVAlignment(a)
    if strupper(a) == self:getVAlignment() then return end
    self.n_text:SetJustifyV(a)
    return self:vAlignmentChanged(a)
end

newClass("Cooldown", Item)
Cooldown:attachMeta("duration")
Cooldown:attachMeta("start")
Cooldown:attachMeta("enabled")
Cooldown.__start = 0
Cooldown.__enabled = 0
Cooldown.__duration = 0

function Cooldown:new(parent)
    Item.new(self, parent, EFrame:newFrame("Cooldown"))
    self.frame:SetSwipeTexture("",1,1,1,1)
    self.frame:SetSwipeColor(0,0,0,0.8)
    self.frame:SetEdgeTexture("Interface\\Cooldown\\edge")
    self.frame:SetBlingTexture("Interface\\Cooldown\\star4", 0.3, 0.6, 1, 0.8)
    local a = bind(function() CooldownFrame_Set(self.frame, self.start, self.duration, self.enabled) end)
    a.parent = self
    a:update()
end

newClass("MouseArea", Item)

MouseArea:attachMeta("pressed")
MouseArea:attachMeta("dragTarget", nil, "setDragTarget")
MouseArea:attachMeta("dragActive")
MouseArea:attachMeta("enabled", "getEnabled", "setEnabled")
MouseArea:attachMeta("containsMouse")
MouseArea:attachMeta("hoover")
MouseArea:attachMeta("containsPress")
MouseArea:attachMeta("mouseX")
MouseArea:attachMeta("mouseY")

function MouseArea:new(parent)
    Item.new(self, parent, EFrame:newFrame("Button"))
    self:attachSignal("clicked")
    self.frame:RegisterForClicks("LeftButtonDown","LeftButtonUp")
    self.frame:SetScript("OnMouseUp", function (this, button)
        self.pressed = false
    end)
    self.frame:SetScript("OnClick", EFrame:makeAtomic(function (this, button)
        if self.frame:GetButtonState() == "NORMAL" then
            local x, y = GetCursorPosition()
            x = x / self.frame:GetEffectiveScale()
            y = - y / self.frame:GetEffectiveScale()
            self.mouseX = x
            self.mouseY = y
            self.clicked(button)
            self.pressed = button
        else
            self.pressed = false
        end
    end))
    local ox, oy
    self.frame:SetScript("OnDragStart", function ()
        local x, y = GetCursorPosition()
        ox = x / self.frame:GetEffectiveScale()
        oy = - y / self.frame:GetEffectiveScale()
        self.dragActive = true
    end)
    self.frame:SetScript("OnDragStop", function (this, button)
        ox = nil
        oy = nil
        self.dragActive = false
        if not self.containsPress or button ~= self.pressed then
            self.pressed = false
        end
    end)
    self.frame:SetScript("OnUpdate", function ()
        if self.pressed or self.hoover then
            local x, y = GetCursorPosition()
            x = x / self.frame:GetEffectiveScale()
            y = - y / self.frame:GetEffectiveScale()
            self.mouseX = x
            self.mouseY = y
            if self.pressed or self.hoover then
                self.containsMouse = MouseIsOver(self.frame) and true or false
            end
            if self.dragActive then
                self.dragTarget.x = self.dragTarget.x + x - ox
                self.dragTarget.y = self.dragTarget.y + y - oy
                ox = x
                oy = y
            end
        end
    end)
    self.enabled = true
    self.hoover = false
    self.dragActive = false
    self.pressed = false
    self.containsPress = bind(function() return self.pressed and self.containsMouse end)
end

function MouseArea:destroy()
    self.frame:SetScript("OnClick",nil)
    self.frame:SetScript("OnMouseUp",nil)
    self.frame:SetScript("OnUpdate",nil)
    self.frame:SetScript("OnDragStart",nil)
    self.frame:SetScript("OnDragStop",nil)
    self.frame:Disable()
    self.frame:RegisterForDrag()
    return Item.destroy(self)
end

function MouseArea:setDragTarget(target)
    if self.dragTarget == target then return end
    
    if not self.dragTarget then
        self.frame:RegisterForDrag("LeftButton")
    elseif not target then
        self.frame:RegisterForDrag()
    end
    self.__dragTarget = target
    return self.dragTargetChanged(target)
end

function MouseArea:getEnabled()
    return self.frame:IsEnabled() == true
end

function MouseArea:setEnabled(e)
    if e == self.enabled then return end
    if e then
        self.frame:Enable()
    else
        self.frame:Disable()
    end
    return self.enabledChanged(e)
end

newClass("Button", MouseArea)

Button:attachMeta("background",nil, "setBackground")
Button:attachMeta("checkable")
Button:attachMeta("checked")
Button:attachMeta("exclusiveGroup")
Button:attachMeta("flat")
Button:attachSignalMeta("toggled")

function Button:new(parent)
    MouseArea.new(self, parent)
    self.flat = false
    self.textLabel = Label(self)
    self._text = self.textLabel._text
    self.background = Rectangle(self)
    self.background.layer = "background"
    self.image = Image(self)
    self.image.layer = "artwork"
    self.background.color = bind(function () return self.flat and {0,0,0,0} or not self.enabled and (not self.checked and {0.5,0.75,0.5} or {.3,.5,.3}) or (self.containsPress or self.checked) and {0,.25,0} or {0,0.75,0} end)
    self.textLabel.anchorFill = self
    self.textLabel.marginLeft = 2
    self.textLabel.marginRight = 2
    self.textLabel.marginTop = bind(function() return self.containsPress and 2 or self.checked and 1 or 0 end)
    self.textLabel.marginBottom = bind(function() return self.containsPress and 1 or self.checked and 2 or 3 end)
    self.textLabel.color = bind(function() return self.enabled and {GameFontNormal:GetTextColor()} or (not self.checked and {0.25,0.25,0.25} or {.66,.66,.66}) end)
    self.implicitWidth = bind(function() return self.textLabel.contentWidth + self.textLabel.marginLeft + self.textLabel.marginRight end)
    self.implicitHeight = bind(function() return self.textLabel.contentHeight + self.textLabel.marginBottom + self.textLabel.marginTop end)
    self.image.anchorFill = self
    self.image.color = bind(function () local up = self.checked or self.containsPress return self.flat and (up and {0,.25,0} or {0,1,0}) or up and {0,1,0} or {0,.25,0} end)
    self._icon = self.image._source
    self.clicked:connect(function ()
        if self.checkable then
            self.checked = not self.checked
            self.toggled()
        end
    end)
    self.checkableChanged:connect(function (c)
        if not c then
            self.checked = false
        end
    end)
    local a = bind(function ()
        local ex = self.exclusiveGroup
        if ex then
            self.checked = ex.current == self
        end
    end)
    a.parent = self
    a:update()
    self.checkedChanged:connect(function (c)
        local ex = self.exclusiveGroup
        if c and ex then
            ex._current:set(self)
        end
    end)
end

function Button:setBackground(f)
    if self.background == f then
        return
    end
    self.__background = f
    if f then
        f.anchorFill = self
    end
    return self.backgroundChanged(f)
end

newClass("ExclusiveGroup", Object)

ExclusiveGroup:attachMeta("current")

function ExclusiveGroup:new(parent)
    Object.new(self, nil, parent)
end

newClass("CheckButton", MouseArea)

CheckButton:attachMeta("checked")
CheckButton:attachMeta("exclusiveGroup")
CheckButton:attachSignalMeta("toggled")

function CheckButton:new(parent)
    MouseArea.new(self, parent)
    self.square = Rectangle(self)
    self.square.color = {0,1,0,0.25}
    self.square.borderWidth = 2
    self.square.borderColor = bind(function() return self.enabled and {0,1,0} or {.25,.75,.25} end)
    self.square.anchorVCentre = self.vcentre
    self.tick = Rectangle(self.square)
    self.tick.anchorFill = self.square
    self.tick.margins = 4
    self.label = Label(self)
    self.label.anchorLeft = self.square.right
    self.label.anchorVCentre = self.vcentre
    self.label.marginLeft = 2
    self.label.color = bind(function() return self.enabled and {GameFontNormal:GetTextColor()} or {.25,.25,.25} end)
    self._text = self.label._text
    self.checked = false
    self.square.implicitHeight = self.label._height
    self.square.implicitWidth = self.square._height
    self.implicitWidth = bind(function() return self.square.width + self.label.width + self.label.marginLeft end)
    self.implicitHeight = bind(function() return max(self.square.height, self.label.height) end)
    self.tick.color = bind(function() return self.containsPress and {0.25,.75,0.25} or self.checked and (self.enabled and {0,1,0} or {.25,.75,.25}) or {0,0,0,0} end)
    self.clicked:connect(function ()
        self.checked = not self.checked
        self.toggled()
    end)
    local a = bind(function ()
        local ex = self.exclusiveGroup
        if ex then
            self.checked = ex.current == self
        end
    end)
    a.parent = self
    a:update()
    self.checkedChanged:connect(function (c)
        local ex = self.exclusiveGroup
        if c and ex then
            ex._current:set(self)
        end
    end)
end

newClass("RadioButton", MouseArea)

RadioButton:attachMeta("checked")
RadioButton:attachMeta("exclusiveGroup")
RadioButton:attachSignalMeta("toggled")

function RadioButton:new(parent)
    MouseArea.new(self, parent)
    self.square = Rectangle(self)
    self.square.color = {0,1,0,0.25}
    self.square.borderWidth = 2
    self.square.borderColor = bind(function() return self.enabled and {0,1,0} or {.25,.75,.25} end)
    self.square.anchorVCentre = self.vcentre
    self.tick = Rectangle(self.square)
    self.tick.margins = 4
    self.tick.anchorFill = self.square
    self.label = Label(self)
    self.label.anchorLeft = self.square.right
    self.label.anchorVCentre = self.vcentre
    self.label.marginLeft = 2
    self.label.color = bind(function() return self.enabled and {GameFontNormal:GetTextColor()} or {.25,.25,.25} end)
    self._text = self.label._text
    self.checked = false
    self.square.implicitHeight = bind(function() return self.label.height end)
    self.square.implicitWidth = bind(function() return self.square.height end)
    self.implicitWidth = bind(function() return self.square.width + self.label.width + self.label.marginLeft end)
    self.implicitHeight = bind(function() return max(self.square.height, self.label.height) end)
    self.tick.color = bind(function() return self.containsPress and {0.25,.75,0.25} or self.checked and (self.enabled and {0,1,0} or {.25,.75,.25}) or {0,0,0,0} end)
    self.clicked:connect(function ()
        self._checked:set(true)
        self.toggled()
    end)
    local a = bind(function ()
        local ex = self.exclusiveGroup
        if ex then
            self._checked:set(ex.current == self)
        end
    end)
    a.parent = self
    a:update()
    self.checkedChanged:connect(function (c)
        local ex = self.exclusiveGroup
        if c and ex then
            ex._current:set(self)
        end
    end)
end

newClass("Flickable", Item)

Flickable:attachMeta("contentItem", nil, "setContentItem")
Flickable:attachMeta("contentWidth")
Flickable:attachMeta("contentHeight")
Flickable:attachMeta("contentX", nil, "setContentX")
Flickable:attachMeta("contentY", nil, "setContentY")

function Flickable:new(parent, item)
    Item.new(self, parent, EFrame:newFrame("ScrollFrame"))
    self.frame:EnableMouseWheel(1)
    self.frame:SetScript("OnMouseWheel", function (this, delta)
        if IsShiftKeyDown() then
            local target, max = this:GetHorizontalScroll() - 25 * delta, this:GetHorizontalScrollRange()
            if target < 0 then
                target = 0
            elseif target > max then
                target = max
            end
            this:SetHorizontalScroll(target)
        else
            local target, max = this:GetVerticalScroll() - 25 * delta, this:GetVerticalScrollRange()
            if target < 0 then
                target = 0
            elseif target > max then
                target = max
            end
            this:SetVerticalScroll(target)
        end
    end)
    self.contentWidthChanged:connect(function(a) self.frame:UpdateScrollChildRect() end)
    self.contentHeightChanged:connect(function(a) self.frame:UpdateScrollChildRect() end)
    self.contentWidth = bind("self.contentItem and self.contentItem.width or 0")
    self.contentHeight = bind("self.contentItem and self.contentItem.height or 0")
end

function Flickable:destroy()
    self.frame:SetScrollChild(nil)
    self.frame:EnableMouseWheel(0)
    self.frame:SetScript("OnMouseWheel", nil)
    return Item.destroy(self)
end

function Flickable:setContentItem(citem)
    if citem == self._contentItem then return end
    self.__contentItem = citem
    self.frame:SetScrollChild(citem.frame)
    return self.contentItemChanged(citem)
end

function Flickable:setContentX(x)
    if fuzzyCmp(x, self.__contentX) then return end
    self.frame:SetHorizontalScroll(x)
    self.__contentX = x
    return self.contentXChanged(x)
end

function Flickable:setContentY(y)
    if fuzzyCmp(y, self.__contentY) then return end
    self.frame:SetVerticalScroll(y)
    self.__contentY = y
    return self.contentYChanged(y)
end

newClass("TextArea", Flickable)

TextArea:attachMeta("text", nil,"setText")
TextArea:attachMeta("focus", nil, "setFocus")
TextArea:attachMeta("color",nil,"setColor")
TextArea:attachMeta("multiLine",nil,"setMultiLine")
TextArea:attachMeta("numeric",nil,"setNumeric")
TextArea:attachMeta("shadowColor","getShadowColor","setShadowColor")
TextArea:attachMeta("shadowOffset","getShadowOffset","setShadowOffset")
TextArea:attachMeta("outline","getOutline","setOutline")
TextArea:attachMeta("justifyH","getJustifyH","setJustifyH")
TextArea:attachMeta("justifyV","getJustifyV","setJustifyV")
TextArea:attachMeta("readOnly")
TextArea:attachSignalMeta("enterPressed")
TextArea.__multiLine = true

function TextArea:new(parent)
    Flickable.new(self, parent)
    self.n_text = EFrame:newFrame("EditBox", self.frame)
    self.n_text:SetFontObject("ChatFontNormal")
    self.n_text:SetMultiLine(1)
    self.n_text:SetAutoFocus(false)
    self.contentItem = Item(self, self.n_text)
    self.contentItem.width = bind(function() return math.max(self.width, self.contentItem.implicitWidth) end)
    self.contentItem.height = bind(function() return math.max(self.height, self.contentItem.implicitHeight) end)
    self.text = ""
    self.readOnly = false
    self.__focus = false
    self.n_text:SetScript("OnTextChanged", function(this) if self.readOnly then this:SetText(self.__text) else self:setText(this:GetText()) end end)
    self.n_text:SetScript("OnKeyDown", function(this) this:SetPropagateKeyboardInput(self.readOnly) end)
    self.n_text:SetScript("OnKeyUp", function(this) this:SetPropagateKeyboardInput(self.readOnly) end)
    self.n_text:SetScript("OnEditFocusGained", function() self.focus = true end)
    self.n_text:SetScript("OnEditFocusLost", function() self.focus = false end)
    self.n_text:SetScript("OnUpdate",function(this) if this:GetHeight()~= 0 then self.contentItem.height = this:GetHeight() this:SetScript("OnUpdate",nil)  end end)
    self.n_text:HookScript("OnEnterPressed", function(this) if self.__multiLine then this:Insert("\n") else self.enterPressed() end end)
    self.n_text:SetScript("OnEscapePressed", function(this) this:ClearFocus() end)
end

function TextArea:destroy()
    self.n_text:SetScript("OnTextChanged", nil)
    self.n_text:SetScript("OnKeyDown", nil)
    self.n_text:SetScript("OnKeyUp", nil)
    self.n_text:SetScript("OnEditFocusGained", nil)
    self.n_text:SetScript("OnEditFocusLost", nil)
    self.n_text:SetNumeric(false)
    self.n_text:SetMultiLine(false)
    self.contentItem:destroy()
    return Flickable.destroy(self)
end

function TextArea:getText()
    return self.n_text:GetText()
end

function TextArea:setText(t)
    if t == self.__text then return end
    self.__text = t
    self.n_text:SetText(t)
    self.textChanged(t)
    return self:refreshContentSize()
end

function TextArea:refreshContentSize()
    local v = self.n_text:GetHeight()
    if self.contentHeight ~= v then
        self.contentItem.height = v
    end
end

function TextArea:setFocus(f)
    if self.focus == f then return end
    self.__focus = f
    if f then
        self.n_text:SetFocus()
    else
        self.n_text:ClearFocus()
    end
    return self.focusChanged(f)
end

function TextArea:setColor(color)
    if colorComp(self.__color, color) then
        return
    end
    self.__color = color
    self.n_text:SetTextColor(unpack(color))
    return self.colorChanged(color)
end

function TextArea:getShadowColor()
    return {self.n_text:GetShadowColor()}
end
    
function TextArea:setShadowColor(c)
    local old = self:getShadowColor()
    if colorComp(old, c) then return end
    self.n_text:SetShadowColor(unpack(c))
    return self.shadowColorChanged(c)
end
    
function TextArea:setMultiLine(m)
    if self.__multiLine == m then return end
    self.n_text:SetMultiLine(m)
    self.__multiLine = m
    return self.multiLineChanged(m)
end
    
function TextArea:setNumeric(n)
    if self.__numeric == n then return end
    self.n_text:SetNumeric(n)
    return self.numericChanged(c)
end

Slider = {}

function TextArea:selectText(p0, p1)
    self.n_text:HighlightText(p0,p1)
end

newClass("Layout", Item)
newClass("RowLayout", Layout)
newClass("SpinBox", RowLayout)

SpinBox:attachMeta("to")
SpinBox:attachMeta("from")
SpinBox:attachMeta("step")
SpinBox:attachMeta("value")
SpinBox:attachMeta("integer")
SpinBox:attachSignalMeta("valueModified")
SpinBox.__value = 0
SpinBox.__step = 1

function SpinBox:new(parent)
    RowLayout.new(self, parent)
    self.less = Button(self)
    self.less.text = "-"
    self.text = TextArea(self)
    self.text.multiLine = false
    self.text.numeric = self._integer
    self.text.implicitWidth = 30
    self.text.Layout.fillHeight = true
    self.text.Layout.fillWidth = true
    self.text.text = bind(function() return self.value end)
    self.text.hAlignment = "RIGHT"
    self.more = Button(self)
    self.more.text = "+"
    self.less.enabled = bind(function() return not self.from or self.value ~= self.from end)
    self.more.enabled = bind(function() return not self.to or self.value ~= self.to end)
    self.less.clicked:connect(function() local f = math.floor((self.__value - self.__step)*1000+0.5)/1000 if self.__from and f < self.__from then f = self.__from end self._value:set(f) self.valueModified(f) end)
    self.more.clicked:connect(function() local f = math.floor((self.__value + self.__step)*1000+0.5)/1000 if self.__to and f > self.__to then f = self.__to end self._value:set(f) self.valueModified(f) end)
    self.text.enterPressed:connect(function() local n = tonumber(self.text.__text) if n then self._value:set(n) self.valueModified(self.__value) end end)
end
    

newClass("Window", Item)

Window:attachMeta("background",nil, "setBackground")
Window:attachMeta("centralItem", nil, "setCentralItem")
Window:attachMeta("padding")
Window.__padding = 0

function Window:new(parent)
    Item.new(self, parent)
    self._marea = MouseArea(self)
    self._marea.dragTarget = self
    self.decoration = Rectangle(self._marea)
    self.decoration.anchorTop = self._marea.top
    self.decoration.anchorLeft = self._marea.left
    self.decoration.anchorRight = self._marea.right
    self.decoration.color = {0,1,0,0.5}
    self.decoration.layout = RowLayout(self.decoration)
    self.decoration.layout.margins = 2
    self.decoration.layout.anchorFill = self.decoration
    self.titleLabel = Label(self.decoration.layout)
    self.titleLabel.Layout.alignment = Layout.AlignVCenter
    self.titleLabel.Layout.fillWidth = true
    self.minimizeButton = Button(self.decoration.layout)
    self.minimizeButton.Layout.fillHeight = true
    self.minimizeButton.flat = true
    self.minimizeButton.implicitWidth = bind(function() return self.minimizeButton.height end)
    self.minimizeButton.checkable = true
    self.minimizeButton.icon = "Interface\\Addons\\EmeraldFramework\\Textures\\MinimizeButton"
    self.minimizeButton.checkedChanged:connect(function(c) self.centralItem.visible = not c end)
    self.closeButton = Button(self.decoration.layout)
    self.closeButton.Layout.fillHeight = true
    self.closeButton.flat = true
    self.closeButton.implicitWidth = bind(function() return self.closeButton.height end)
    self.closeButton.icon = "Interface\\Addons\\EmeraldFramework\\Textures\\CloseButton"
    self.closeButton.clicked:connect(function() self.visible = false end)
    self.decoration.height = bind(function() return max(20, self.titleLabel.contentHeight) end)
    self.background = Rectangle(self)
    self._title = self.titleLabel._text
    self.textChanged = self.titleLabel._textChanged
    self.visibleChanged:connect(function (v)
        if v then
            self.minimizeButton.checked = false
        end
    end)
    self.background.borderColor = {0,1,0,0.5}
    self.background.color = {0,1,0,0.25}
    self.background.borderWidth = 2
    self.background.margins = bind(function() return -self.background.borderWidth -self.padding end)
    self.background.visible = bind(function() return not self.centralItem or self.centralItem.visible end)
    self.resize_frame = Button(self._marea)
    self.resize_frame.icon = "Interface\\Addons\\EmeraldFramework\\Textures\\ResizeHandle_small"
    self.resize_frame.pressedChanged:connect(function(p)
        if p then
            self.resize_frame.ox = self.resize_frame.mouseX
            self.resize_frame.oy = self.resize_frame.mouseY
        end
    end)
    self.resize_frame.mouseXChanged:connect(function (x)
        if self.resize_frame.__pressed then
            self.width = math.max(self.__width + (x - self.resize_frame.ox), self.centralItem.implicitWidth)
            self.resize_frame.ox = x
        end
    end)
    self.resize_frame.mouseYChanged:connect(function (y)
        if self.resize_frame.__pressed then
            self.height = math.max(self.__height + (y - self.resize_frame.oy), self.centralItem.implicitHeight)
            self.resize_frame.oy = y
        end
    end)
    
    self.resize_frame.width = 14
    self.resize_frame.height = 14
    self.resize_frame.anchorBottom = self._marea.bottom
    self.resize_frame.anchorRight = self._marea.right
    self.resize_frame.visible = bind(function() return self.centralItem and self.centralItem.visible end)
    self._marea.anchorFill = self
    self._marea.marginTop = bind(function() return -self.decoration.height - self.background.borderWidth - self.padding end)
    self._marea.marginBottom = bind(function() return (self.centralItem and not self.centralItem.visible and self.centralItem.height or -self.background.borderWidth)  - self.padding end)
    self._marea.marginLeft = bind(function() return -self.background.borderWidth - self.padding end)
    self._marea.marginRight = bind(function() return -self.background.borderWidth - self.padding end)
    self.x = 2
    self.y = 30
    self.implicitWidth = bind(function() return math.max(self.centralItem and self.centralItem.implicitWidth or 0, self.decoration.layout.implicitWidth) end)
    self.implicitHeight = bind(function() return self.centralItem and self.centralItem.implicitHeight or 0 end)
end

function Window:setBackground(f)
    if self.background == f then
        return
    end
    self.__background = f
    if f then
        f.anchorFill = self
    end
    return self.backgroundChanged(f)
end

function Window:setCentralItem(f)
    if f == self.centralItem then return end
    if f then
        f.anchorFill = self
    end
    self.__centralItem = f
    return self.centralItemChanged(f)
end

rootFrame:SetScale(WorldFrame:GetHeight()/resy)
root = Item(nil, rootFrame, false)
root.name = "EFFrame.root"
root.width = resx
root.height = resy

