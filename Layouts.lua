local EF
if EFrame == nil then 
    EFrame = {}
end
EF = EFrame

setmetatable(EF, {__index = getfenv() })
setfenv(1, EF)


Layout.AlignLeft = 1
Layout.AlignRight = 2
Layout.AlignHCenter = 4
Layout.AlignTop = 32
Layout.AlignBottom = 64
Layout.AlignVCenter = 128
Layout.AlignCenter = bit.bor(Layout.AlignHCenter, Layout.AlignVCenter)
Layout.AlignHorizzontalMask = 15
Layout.AlignVerticalMask = 240

Layout:attachMeta("hasFillWidth")
Layout:attachMeta("hasFillHeight")

function Layout:new(parent)
    Item.new(self, parent)
end

function Layout:addChildItem(c)
    c.Layout = LayoutAttached(c)
    VirtualItem.addChildItem(self, c)
end

function Layout:removeChildItem(c)
    VirtualItem.removeChildItem(self, c)
    c.Layout:destroy()
    c.Layout = nil
end

newClass("LayoutAttached", Object)

LayoutAttached:attachMeta("alignment")
LayoutAttached:attachMeta("fillWidth")
LayoutAttached:attachMeta("fillHeight")

newClass("GridLayout", Layout)


function GridLayout:new(nr, nc, parent)
    self.__rowSpacing = 0
    self.__columnSpacing = 0
    nr, nc = nr or 0, nc or 0
    Layout.new(self, parent)
    self:attach("rows")
    self:attach("columns")
    self:attach("rowSpacing")
    self:attach("columnSpacing")
    self:attach("grid")
    self.rows = nr
    self.columns = nc
    self.grid = {}
    local a = bind(function() self:refreshLayout() end)
    a.parent = parent
    a:update()
end

function GridLayout:refreshLayout()
--     local cc, r, c = self.children, self.rows, self.columns
--     local rs, cs = self.rowSpacing, self.columnSpacing
--     if #cc == 0 then return end
--     local cy = 0
--     for i = 0, r-1 do
--         local max = 0
--         for j = 0, c-1 do
--             local f = cc[j + c*i +1]
--             if f then
--                 local ih = f.height
--                 if ih > max then
--                     max = ih
--                 end
--             end
--         end
--         for j = 0, c -1 do
--             local f = cc[j + c*i +1]
--             if f then
--                 f.y = cy
--             end
--         end
--         cy = cy + rs + max
--     end
--     local cx = 0
--     for i = 0, c -1 do
--         local max = 0
--         for j = 0, r -1 do
--             local f = cc[i + c*j +1]
--             if f then
--                 local iw = f.width
--                 if iw > max then
--                     max = iw
--                 end
--             end
--         end
--         for j = 0, r -1 do
--             local f = cc[i + c*j +1]
--             if f then
--                 f.x = cx
--             end
--         end
--         cx = cx + cs + max
--     end
--     self.implicitWidth = cx
--     self.implicitHeight = cy
end



function RowLayout:new(parent)
    self.__spacing = 0
    Layout.new(self, parent)
    self:attach("spacing")
    self.implicitHeight = bind(RowLayout.getImplicitHeight)
    self.implicitWidth = bind(RowLayout.getImplicitWidth)
    local a = bind(RowLayout.refreshLayoutX)
    a.parent = self
    a:update()
    local b = bind(RowLayout.refreshLayoutY)
    b.parent = self
    b:update()
end

function RowLayout:getImplicitHeight()
    local cc = self.visibleChildren
    if #cc == 0 then return 0 end
    local mh = 0
    for _,f in ipairs(cc) do
        if not f.implicitHeight then print(f.type) end
        mh = math.max(f.implicitHeight, mh)
    end
    return mh
end

function RowLayout:getImplicitWidth()
    local cc = self.visibleChildren
    local spacing = self.spacing
    if #cc == 0 then return 0 end
    local cx = 0
    for k, f in ipairs(cc) do
        cx = cx + f.implicitWidth + (k == #cc and 0 or spacing)
    end
    return cx
end

function RowLayout:refreshLayoutX()
    local cc = self.visibleChildren
    local spacing = self.spacing
    local nc = #cc
    local tex = self.width - self.implicitWidth
    if nc == 0 then return end
    local cx, fw = 0, 0
    for _,f in ipairs(cc) do
        if f.Layout.fillWidth then
            fw = fw + 1
        end
    end
    for _,f in ipairs(cc) do
        local ex = fw == 0 and tex/nc or f.Layout.fillWidth and tex/fw or 0
        local ha = not f.Layout.alignment and 0 or bit.band(f.Layout.alignment, Layout.AlignHorizzontalMask)
        if not f.implicitWidth then print(f.type) end
        local width = f.implicitWidth + ex
        if f.Layout.fillWidth then
            f.width = width
            f.x = cx
        else
            f.x = cx + (ha == Layout.AlignHCenter and 0 or
                    ha == Layout.AlignRight and ex or ex/2)
        end
        cx = cx + width + spacing
    end
end

function RowLayout:refreshLayoutY()
    local cc = self.visibleChildren
    for _,f in ipairs(cc) do
        if f.Layout.fillHeight then
            f.height = self.height
        else
            local va = not f.Layout.alignment and 0 or bit.band(f.Layout.alignment, Layout.AlignVerticalMask)
            f.y = va == Layout.AlignVCenter and (self.height - f.height)/2 or
                va == Layout.AlignBottom and self.height - f.height or 0
        end
    end
end

newClass("ColumnLayout", Layout)

function ColumnLayout:new(parent)
    self.__spacing = 0
    Layout.new(self, parent)
    self:attach("spacing")
    self.implicitHeight = bind(ColumnLayout.getImplicitHeight)
    self.implicitWidth = bind(ColumnLayout.getImplicitWidth)
    local a = bind(ColumnLayout.refreshLayoutX)
    a.parent = self
    a:update()
    local b = bind(ColumnLayout.refreshLayoutY)
    b.parent = self
    b:update()
end

function ColumnLayout:getImplicitHeight()
    local cc = self.visibleChildren
    local spacing = self.spacing
    if #cc == 0 then return 0 end
    local cy = 0
    for k, f in ipairs(cc) do
        cy = cy + f.implicitHeight + (k == #cc and 0 or spacing)
    end
    return cy
end

function ColumnLayout:getImplicitWidth()
    local cc = self.visibleChildren
    if #cc == 0 then return 0 end
    local mw = 0
    for _,f in ipairs(cc) do
        mw = math.max(f.implicitWidth, mw)
    end
    return mw
end

function ColumnLayout:refreshLayoutX()
    local cc = self.visibleChildren
    for _,f in ipairs(cc) do
        if f.Layout.fillWidth then
            f.width = self.width
        else
            local ha = not f.Layout.alignment and 0 or bit.band(f.Layout.alignment, Layout.AlignHorizzontalMask)
            f.x = ha == Layout.AlignHCenter and (self.width - f.width)/2 or
                ha == Layout.AlignRight and self.width - f.width or 0
        end
    end
end 

function ColumnLayout:refreshLayoutY()
    local cc = self.visibleChildren
    local spacing = self.spacing
    local nc = #cc
    local tex = self.height - self.implicitHeight
    if nc == 0 then return end
    local cy, fh = 0, 0
    for _,f in ipairs(cc) do
        if f.Layout.fillHeight then
            fh = fh + 1
        end
    end
    for _,f in ipairs(cc) do
        local ex = fh == 0 and tex/nc or f.Layout.fillHeight and tex/fh or 0
        local va = not f.Layout.alignment and 0 or bit.band(f.Layout.alignment, Layout.AlignVerticalMask)
        local height = f.implicitHeight + ex
        if f.Layout.fillHeight then
            f.height = height
            f.y = cy
        else
            f.y = cy + (va == Layout.AlignTop and 0 or
                    va == Layout.AlignBottom and ex or ex/2)
        end
        cy = cy + height + spacing
    end
end 
